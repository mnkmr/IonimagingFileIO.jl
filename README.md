# IonimagingFileIO

[![Build Status](https://gitlab.com/mnkmr/IonimagingFileIO.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/IonimagingFileIO.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/IonimagingFileIO.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/IonimagingFileIO.jl/commits/master)

A [Julia](https://julialang.org/) package to read HDF5 files created by [Ionimaging](https://gitlab.com/mnkmr/Ionimaging.jl) package.


## License

[MIT License](LICENSE)


## Installation

Open a julia REPL and press `]` key to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/#Getting-Started-1). Then execute the following command.

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add IonimagingFileIO
```

If you have already added the [MNkmrRegistry](https://gitlab.com/mnkmr/MNkmrRegistry) before, you can omit the first line.


## Usage

First of all, load this package.

```
julia> using IonimagingFileIO
```

### Read a file

`loadimg()` function is exported to load an image from a file. Replace `path/to/file` with an appropriate file path.

```
julia> img = loadimg("path/to/file")
```

Note that use `\\` instead of `\` as a path separater in double quatation on Windows OS.

```
julia> img = loadimg("C:\\Users\\username\\Documents\\2020\\0101\\1.hdf5")
```

Or, use *raw string* syntax to avoid escaping.

```
julia> img = loadimg(raw"C:\Users\username\Documents\2020\0101\1.hdf5")
```
