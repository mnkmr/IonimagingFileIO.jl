using Base: Filesystem
using IonimagingImageTypes: Rawimage, Ionimage, Ionspots
using IonimagingFileIO
using Test

const testimage1 = [ 0 0 0 0 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 2 3 2 0 0 0 0 0
                     0 1 2 1 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0
                     0 0 0 0 0 0 0 0 0 ]
const testspots1 = [(2.0, 2.0),
                    (3.0, 2.0),
                    (4.0, 2.0),
                    (2.0, 3.0),
                    (3.0, 3.0),
                    (4.0, 3.0),
                    (2.0, 4.0),
                    (3.0, 4.0),
                    (4.0, 4.0),
                    (3.0, 2.0),
                    (2.0, 3.0),
                    (3.0, 3.0),
                    (4.0, 3.0),
                    (3.0, 4.0),
                    (3.0, 3.0)]

const shape = size(testimage1)

@testset "IonimagingFileIO.jl" begin
    tempdir = Filesystem.tempdir()
    tempfile = joinpath(tempdir, "test1.hdf5")

    try
        img = Rawimage(testimage1)
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = loadimg(tempfile)
        @test img.arr == fromfile.arr
        Filesystem.rm(tempfile, force=false)

        img = Ionimage(testimage1)
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = loadimg(tempfile)
        @test img.arr == fromfile.arr
        Filesystem.rm(tempfile, force=false)

        img = Ionspots(testspots1, shape...)
        saveimg(tempfile, img)
        @test isfile(tempfile)
        fromfile = loadimg(tempfile)
        @test img.arr == fromfile.arr
        Filesystem.rm(tempfile, force=false)
    finally
        Filesystem.rm(tempfile, force=true)
    end
end
