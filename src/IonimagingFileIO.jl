module IonimagingFileIO

export saveimg, loadimg

using IonimagingImageTypes: AbstractIonimage, Rawimage, Ionimage, Ionspots, develop
using Dates: DateFormat, DateTime
using HDF5: h5open, attributes, read, names


"""
    saveimg(filepath, ionimage::AbstractIonimage; complv=1, force=false)

Save an image.

The image is saved in a HDF5 container format. The image data is stored at the
root of container with "image" tag, and the metadata is saved as its attributes.

This function compress the data using ZLIB (deflate algorithm). User can assign
the compression level through `complv`; 0 (no compression) to 9.

In default, this function fails if the `filepath` already exists. If `force` is
true, the current file will be overwritten forcibly.

# Examples
```julia-repl
julia> raw = take();

julia> saveimg("image.h5", raw)

julia> img = ionimage(100, 240)

julia> saveimg("image.h5", raw)  # Fail
┌ Error: 'image.h5' already exists, failed to save.
└ @ Ionimaging
```
"""
function saveimg(filepath, ionimage::AbstractIonimage; complv=1, force=false)
    if !force && isfile(filepath)
        @error "'$(filepath)' already exists, failed to save."
        return
    end

    h5open(filepath, "w") do file
        file["image", compress=complv] = ionimage.arr

        if isdefined(ionimage, :spots)
            spot_y, spot_x = split_to_arrays(ionimage.spots, Float64)
            file["spot_y", compress=complv] = spot_y
            file["spot_x", compress=complv] = spot_x
        end

        for (k, v) in ionimage.meta
            if k == "starttime" || k == "finishtime"
                attributes(file["image"])[k] = "$(v)"
            elseif k == "mask"
                mask_y, mask_x = split_to_arrays(v, Int)
                attributes(file["image"])["masked_pixel_y"] = mask_y
                attributes(file["image"])["masked_pixel_x"] = mask_x
            else
                attributes(file["image"])[k] = v
            end
        end
    end
end


"""
    loadimg(filepath)

Read out an image from the file `filepath`.

# Examples
```julia-repl
julia> raw = take();

julia> saveimg("image.h5", raw)

julia> raw = loadimg("image.h5")
```
"""
function loadimg(filepath)
    if !isfile(filepath)
        throw(SystemError("opening file \"$(filepath)\": No such file"))
    end

    h5open(filepath, "r") do file
        arr = read(file, "image")

        df = DateFormat("y-m-dTH:M:S.s")
        imageattr = attributes(file["image"])
        meta = Dict{String,Any}()
        for k in keys(imageattr)
            if k == "starttime" || k == "finishtime"
                timestr = read(imageattr, k)
                meta[k] = DateTime(timestr, df)
            elseif k == "masked_pixel_y"
                mask_y = read(imageattr, "masked_pixel_y")
                mask_x = read(imageattr, "masked_pixel_x")
                meta["mask"] = merge_arrays(mask_y, mask_x, Int)
            elseif k == "masked_pixel_x"
                # Skip here
                # All we need has done in 'masked_pixel_y' key
            else
                meta[k] = read(imageattr, k)
            end
        end

        t = get(meta, "type", "")
        if t == "Ionspots"
            spot_y = read(file, "spot_y")
            spot_x = read(file, "spot_x")
            spots = merge_arrays(spot_y, spot_x, Float64)
            Ionspots(spots, arr, meta)
        elseif t == "Ionimage"
            Ionimage(arr, meta)
        else
            Rawimage(arr, meta)
        end
    end
end


function split_to_arrays(tuplelist, T)
    if isempty(tuplelist)
        yarray = T[]
        xarray = T[]
    else
        yarray = map(t -> T(t[1]), tuplelist)
        xarray = map(t -> T(t[2]), tuplelist)
    end
    yarray, xarray
end


function merge_arrays(yarray, xarray, T)
    tuplelist = NTuple{2,T}[]
    for coord in zip(yarray, xarray)
        push!(tuplelist, coord)
    end
    tuplelist
end

end # module
